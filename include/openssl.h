//
//  base64 encoding and decoding with C++.
//  Version: 1.01.00
//

#ifndef OPENSSL_H_INCLUDED
#define OPENSSL_H_INCLUDED

int encrypt(unsigned char *plaintext, int plaintext_len, unsigned char *key,
            unsigned char *ciphertext);
int decrypt(unsigned char *ciphertext, int ciphertext_len, unsigned char *key,
            unsigned char *plaintext);

#endif /* OPENSSL_H_INCLUDED */
