#!/bin/false
# Functions taking care of the cURL call

# Retrieve OpenSSL certificates
SCRIPT_FOLDER=${SCRIPT_FOLDER:-/usr/lib}
source "${SCRIPT_FOLDER}/mydatakeeper-certificates" 2>/dev/null

# Set cURL arguments
CURL_ARGS+=('-s')
CURL_ARGS+=('--key' "$KEY")
CURL_ARGS+=('--cert' "$PRIMARY_CERT") # TODO: fallback to secondary certificate

code=-1
message="No call done"
meta=

account_api_call() {
    # Create ouput file
    local OUTPUT=$(/bin/mktemp)
    local HEADERS=$(/bin/mktemp)

    ## Execute request
    if ! /usr/bin/curl \
            "${CURL_ARGS[@]}" \
            --output "$OUTPUT" \
            --dump-header "$HEADERS" \
            -X "$@" >/dev/null
    then
        code=-1
        message="Internal error"
        /bin/rm -f "$OUTPUT" "$HEADERS"
        return
    fi

    local CONTENT_TYPE=$(/bin/grep -i '^content-type:' "$HEADERS" | /usr/bin/cut -d' ' -f2- | /usr/bin/tr -d '[[:space:]]')
    if [ "$CONTENT_TYPE" != "application/json" ]; then
        code=$(/bin/grep '^HTTP' "$HEADERS" | /usr/bin/cut -d' ' -f2 | /usr/bin/tr -d '[[:space:]]')
        message=$(/bin/grep '^HTTP' "$HEADERS" | /usr/bin/cut -d' ' -f3-)
        /bin/rm -f "$OUTPUT" "$HEADERS"
        return
    fi

    if ! code=$(/usr/bin/jq -je .code "$OUTPUT") \
    || ! message=$(/usr/bin/jq -je .message "$OUTPUT")
    then
        code=500
        message="Malformed answer"
        /bin/rm -f "$OUTPUT" "$HEADERS"
        return
    fi

    meta=$(/usr/bin/jq -je .meta "$OUTPUT" || true)
    /bin/rm -f "$OUTPUT" "$HEADERS"
}

# 0 -> no error
# 1 -> client error
# 2 -> server error
# 3 -> unexpected return code
# * -> internal error
account_api_done() {
    echo "$message"
    >&2 echo "$meta"
    case "$code" in
        1[0-9][0-9])
            exit 3
            ;;
        2[0-9][0-9])
            exit 0
            ;;
        3[0-9][0-9])
            exit 3
            ;;
        4[0-9][0-9])
            exit 1
            ;;
        5[0-9][0-9])
            exit 2
            ;;
        *)
            exit "$code"
            ;;
    esac
}
