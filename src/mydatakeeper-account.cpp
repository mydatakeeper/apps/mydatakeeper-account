#include <iostream>
#include <list>
#include <mutex>
#include <chrono>
#include <thread>
#include <csignal>

#include <mydatakeeper/arguments.h>
#include <mydatakeeper/exec.h>

#include <mydatakeeper/config.h>
#include <mydatakeeper/applications.h>

#include <dbus-c++/dbus.h>

#include <json/json.hh>

#include "base64.h"
#include "openssl.h"

#define NO_ERROR     0
#define CLIENT_ERROR 1
#define SERVER_ERROR 2
#define UNEXPECTED_ERROR 3

using namespace std;

DBus::BusDispatcher dispatcher;

string script_dir;
mutex update_mtx;
bool last_failed = false;
bool retry_after_failure = false;

string decryptMetadata(auto &value, auto &account_number)
{
    const std::string ciphertext = base64_decode(value);

    char key[32] = {0};
    memcpy(
        key, account_number.c_str(),
        account_number.size() >= 32 ? 32 : account_number.size()
    );

    char decryptedtext[4096];
    int decryptedtext_len = decrypt(
        (unsigned char*)ciphertext.c_str(), ciphertext.size(),
        (unsigned char*)key, (unsigned char*)decryptedtext);

    if (!decryptedtext_len)
        return {};

    return string(decryptedtext, decryptedtext_len);
}

inline void setMetadata(auto &account, auto &metadata)
{
    clog << "Setting metadata" << endl;
    account.Set(fr::mydatakeeper::ConfigInterface, "metadata", to_variant<vector<DBus::Struct<string, string>>>(metadata));
}

inline void setStatus(auto &account, auto &value)
{
    clog << "Setting status = " << value << endl;
    account.Set(fr::mydatakeeper::ConfigInterface, "status", to_variant<string>(value));
}

inline void setError(auto &account, auto &value)
{
    clog << "Setting error = " << value << endl;
    account.Set(fr::mydatakeeper::ConfigInterface, "error", to_variant<string>(value));
}

void _update(auto &account,
    const auto &transit, const auto &success, const auto &failure,
    const auto &script, const vector<string> &args, auto callack)
{
    setStatus(account, transit);

    string out, err;
    int ret = execute(script, args, &out, &err);
    clog << "Script " << script << " exited with status " << ret << endl;
    clog << "stdout:\n" << out << endl;
    clog << "stderr:\n" << err << endl;
    switch (ret) {
        case NO_ERROR:
            setStatus(account, success);
            last_failed = false;
            retry_after_failure = false;
            break;
        case CLIENT_ERROR:
            setStatus(account, failure);
            last_failed = true;
            retry_after_failure = false;
            break;
        case SERVER_ERROR:
        case UNEXPECTED_ERROR:
        default:
            setStatus(account, failure);
            last_failed = true;
            retry_after_failure = true;
            break;
    }
    callack(ret, out, err);
    setError(account, out);
}

inline void _update(auto &account,
    const auto &transit, const auto &success, const auto &failure,
    const auto &script, const vector<string> &args = {})
{
    auto callback = [](auto &/*ret*/, auto &/*out*/, auto &/*err*/){};
    _update(account, transit, success, failure, script, args, callback);
}

void update(auto &account, string status = string())
{
    unique_lock update_lck(update_mtx);

    try {
        if (last_failed) {
            if (!retry_after_failure) {
                clog << "No retry needed after last failure. Ignore new state " << status << endl;
                return;
            } else {
                clog << "Waiting 60 secs before retry" << endl;
                std::this_thread::sleep_for(std::chrono::seconds(60));
            }
        }

        if (status.empty()) {
            status = account.Get(fr::mydatakeeper::ConfigInterface, "status").operator string();
        }

        if (status == "init") {
            clog << "Registering certificate" << endl;
            _update(account, "registering", "registered", "init",
                script_dir + "/register-certificate");
            return;
        }

        bool associate = account.Get(fr::mydatakeeper::ConfigInterface, "associate");
        string account_number = account.Get(fr::mydatakeeper::ConfigInterface, "account_number");
        if (status == "registered" && associate && !account_number.empty()) {
            clog << "Associating with account " << account_number << endl;
            _update(account, "associating", "associated", "registered",
                script_dir + "/associate-account", {account_number},
                [&account, &account_number](auto& ret, auto& /*out*/, auto& err){
                    if (ret != 0)
                        return;

                    // Ugly hack : We return meta through stderr
                    const JSON::Array meta = parse_string(err);
                    vector<DBus::Struct<string, string>> metadata;
                    for (const JSON::Object& m : meta) {
                        const auto crypted_value = m["value"].as_string();
                        const auto value = decryptMetadata(crypted_value, account_number);
                        if (value.empty())
                            continue;
                        const auto key = m["type"].as_string();
                        metadata.push_back({key, value});
                    }
                    setMetadata(account, metadata);
                }
            );
            return;
        } else if (status == "associated" && !associate) {
            clog << "Dissociating account" << endl;
            _update(account, "dissociating", "registered", "associated",
                script_dir + "/dissociate-account");
            return;
        }

        bool enable = account.Get(fr::mydatakeeper::ConfigInterface, "enable");
        if (status == "associated" && enable) {
            clog << "Enabling subscription" << endl;
            _update(account, "enabling", "enabled", "associated",
                script_dir + "/enable-subscription");
            return;
        } else if (status == "enabled" && !enable) {
            clog << "Disabling subscription" << endl;
            _update(account, "disabling", "associated", "enabled",
                script_dir + "/disable-subscription");
            return;
        }
    } catch (DBus::Error &e) {
        cerr << "An error occured during account update : " << e.message() << endl;
    }
}

void handler(int sig)
{
    clog << "Stopping DBus main loop: " << strsignal(sig) << endl;
    dispatcher.leave();
}

void bus_type(const string &name, const string &value)
{
    if (value != "system" && value != "session")
        throw invalid_argument(name + "must be either 'system' or 'session'");
}

int main(int argc, char** argv)
{
    string bus, bus_name, bus_path;
    mydatakeeper::parse_arguments(argc, argv, VERSION, {
        {
            bus, "bus", 0, mydatakeeper::required_argument, "system", bus_type,
            "The type of bus used by Dbus interfaces. Either system or session"
        },
        {
            bus_name, "bus-name", 0, mydatakeeper::required_argument, fr::mydatakeeper::ApplicationsName, nullptr,
            "The Dbus name which handles the configuration"
        },
        {
            bus_path, "bus-path", 0, mydatakeeper::required_argument, "/fr/mydatakeeper/account", nullptr,
            "The Dbus path which handles the configuration"
        },
        {
            script_dir, "script-dir", 0, mydatakeeper::required_argument, "/usr/lib/mydatakeeper-account", nullptr,
            "The folder where configuration files will be generated"
        },
    });

    // Setup signal handler
    signal(SIGINT, handler);
    signal(SIGTERM, handler);

    // Setup DBus dispatcher
    DBus::default_dispatcher = &dispatcher;

    // Update variables depending on parameters
    DBus::Connection conn = (bus == "system") ?
        DBus::Connection::SystemBus() :
        DBus::Connection::SessionBus();

    clog << "Getting Config " << bus_path << " proxy" << endl;
    fr::mydatakeeper::ConfigProxy account(conn, bus_path, bus_name);

    clog << "Setting up Config " << bus_path << " update listener" << endl;
    account.onPropertiesChanged = [&account](auto &/*iface*/, auto &changed, auto &/*invalidated*/)
    {
        // Ignore error message updates
        if (changed.size() == 1 && changed.find("error") != changed.end()) {
            return;
        }

        // Ignore transition states
        if (changed.size() == 1 && changed.find("status") != changed.end()) {
            const string &status = changed.at("status");

            static vector<string> transition = {
                "registering",
                "associating", "dissociating",
                "enabling", "disabling",
            };

            if (find(transition.begin(), transition.end(), status) != transition.end()) {
                clog << "Transition to state " << status << endl;
                return;
            }
        }

        clog << "Account config has changed" << endl;
        update(account);
    };

    // Force update during init
    update(account, "init");

    clog << "Starting DBus main loop" << endl;
    dispatcher.enter();

    return 0;
}
