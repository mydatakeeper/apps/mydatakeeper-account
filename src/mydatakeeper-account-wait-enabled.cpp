#include <iostream>
#include <csignal>

#include <mydatakeeper/arguments.h>
#include <mydatakeeper/exec.h>

#include <mydatakeeper/config.h>
#include <mydatakeeper/applications.h>

#include <dbus-c++/dbus.h>

using namespace std;

// Child PID
pid_t pid;

DBus::BusDispatcher dispatcher;

void teardownCaptivePortal()
{
    if (pid != 0) {
        clog << "Killing captive DNS process" << endl;
        if (kill(pid, SIGTERM) == -1) {
            perror("kill");
            exit(EXIT_FAILURE);
        }
        join(pid);
        pid = 0;
    } else {
        clog << "No captive DNS process to kill" << endl;
    }
}

void setupCaptivePortal()
{
    teardownCaptivePortal();
    clog << "Forking and launching captive DNS process" << endl;
    // Will resolv any DNS request to 192.168.10.1
    // which will display the captive portal
    pid = execute_async("/usr/bin/dnsmasq", {
        "--no-daemon",
        "--log-queries",
        "--address=/#/192.168.10.1",
        "--port=53535",
        "--no-hosts",
        "--no-resolv",
        "-i", "lan0",
    });
    if (pid == -1) {
        exit(1);
    }
    clog << "captive DNS process has PID " << pid << endl;
}

void handler(int sig)
{
    clog << "Stopping DBus main loop: " << strsignal(sig) << endl;
    dispatcher.leave();
}

void bus_type(const string &name, const string &value)
{
    if (value != "system" && value != "session")
        throw invalid_argument(name + "must be either 'system' or 'session'");
}

int main(int argc, char** argv)
{
    string bus, bus_name, bus_path;
    mydatakeeper::parse_arguments(argc, argv, VERSION, {
        {
            bus, "bus", 0, mydatakeeper::required_argument, "system", bus_type,
            "The type of bus used by Dbus interfaces. Either system or session"
        },
        {
            bus_name, "bus-name", 0, mydatakeeper::required_argument, fr::mydatakeeper::ApplicationsName, nullptr,
            "The Dbus name which handles the configuration"
        },
        {
            bus_path, "bus-path", 0, mydatakeeper::required_argument, "/fr/mydatakeeper/account", nullptr,
            "The Dbus path which handles the configuration"
        },
    });

    // Setup signal handler
    signal(SIGINT, handler);
    signal(SIGTERM, handler);

    // Setup DBus dispatcher
    DBus::default_dispatcher = &dispatcher;

    // Update variables depending on parameters
    DBus::Connection conn = (bus == "system") ?
        DBus::Connection::SystemBus() :
        DBus::Connection::SessionBus();

    clog << "Getting Config " << bus_path << " proxy" << endl;
    fr::mydatakeeper::ConfigProxy account(conn, bus_path, bus_name);

    clog << "Setting up Account config listener" << endl;
    account.onPropertiesChanged = [](auto &/*iface*/, auto &changed, auto &/*invalidated*/)
    {
        if (changed.find("status") == changed.end())
            return;

        string status = changed.at("status");
        clog << "Status has changed to " << status << endl;
        if (status != "enabled")
            return;

        dispatcher.leave();
    };

    string status = account.Get(fr::mydatakeeper::ConfigInterface, "status");
    clog << "Initial status is " << status << endl;
    if (status == "enabled")
        return 0;

    setupCaptivePortal();
    clog << "Starting DBus main loop" << endl;
    dispatcher.enter();
    teardownCaptivePortal();

    return 0;
}
